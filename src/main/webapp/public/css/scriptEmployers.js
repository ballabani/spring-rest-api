/**
 * Created by bruno on 2.5.2016.
 */
var tempEmployer = {username: ""};
var isCreateEmployer = true;
var table;
/*var temParents = [
    {
        id:"",
        name:"",
        childrens:[
            {
                id:"",
                name:""
            }
        ]
    }
];*/
var tempParents = [];
var temChildren = {};

/**
 * Fill employer table on load
 */
$(document).ready(function () {
    var url = "/rest/employers";
    if (window.location.href.split("=").length > 1) {
        var depName = window.location.href.split("=")[1];
        url = '/rest/employers?departmentName=' + depName;
    }
    $.getJSON(url, function (response) {
        var employers = response;
        employers.forEach(function (empployer) {
            $('#empTableBody').append('<tr>'
                + '<td> ' + empployer.username + '</td>'
                + '<td> ' + empployer.email + '</td>'
                + '<td> ' + empployer.address + '</td>'
                + '<td> ' + empployer.department + '</td>'
                + '<td> ' + empployer.password + '</td>'
                + '<td> ' + empployer.role + '</td>'
                + '<td><button class="btn btn-primary" id="updateEmployer" onclick="updateEmployerModal()" aria-hidden="true" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-pencil-square-o"></i></button></td>'
                + '<td><button class="btn btn-danger " id="deleteEmployer" data-toggle="modal" data-target=".bs-example-modal-sm" onclick="deleteEmployerModal()"><i class="fa fa-trash" aria-hidden="true"></i></button></td></tr>'
            );
        });
        table = $('#employersTable').DataTable({
            "columnDefs": [
                {"visible": false, "targets": 4}
            ]
        });
    });
    /*Get departments*/
    $.getJSON('/rest/department', function (response) {
        var departaments = response;
        departaments.forEach(function (department) {
            $('#employerDepartment').append('<option value = "'+department.name+'">'+department.name+'</option>');
        });
    });
    // Set the global configs to synchronous 
    $.ajaxSetup({
        async: false
    });
    /* Get departments */
    $.getJSON('/rest/department', function (response) {
        var departaments = response;
        departaments.forEach(function (department) {
            /*$('#mytree').append('<li id="'+department.id+'" class="parent">' + department.name + '<ul></ul></li>');*/
            tempParents.push({
                id: department.id,
                name: department.name,
                childrens: []
            });
        });
    });

    tempParents.forEach(function (department) {
        /* Get subDepartment */
        $.getJSON('/rest/getSubDepartment?departmentName=' + department.name, function (response) {
            var subDepartments = response;
            subDepartments.forEach(function (subDepartment) {
                /*$('#'+department.id + " ul").append('<li data-jstree=\'{"icon":"glyphicon glyphicon-briefcase"}\' id="_'+subDepartment.id+'">' + subDepartment.name + '</li>');*/
                department.childrens.push({
                    id: subDepartment.id,
                    name: subDepartment.name
                })
            });
        });
    });
    // Set the global configs to synchronous 
    $.ajaxSetup({
        async: true
    });
    
    console.warn(tempParents);
    
    tempParents.forEach(function (department) {
        $('#mytree').append('<li id="'+department.id+'" class="parent">' + department.name + '<ul></ul></li>');
        department.childrens.forEach(function (subDepartment) {
            $('#'+department.id + " ul").append('<li data-jstree=\'{"icon":"glyphicon glyphicon-briefcase"}\' id="_'+subDepartment.id+'">' + subDepartment.name + '</li>');
        })
    });
    // RENDER TREE
    // 6 create an instance when the DOM is ready
    $('#jstree').jstree();
    // 7 bind to events triggered on the tree
    $('#jstree').on("changed.jstree", function (e, data) {
        console.log(data.node.text);
        getEmployersOnTreeClick(data.node.text);
    });
});

/**
 * Open delete employer modal
 */
var deleteEmployerModal = function () {
    isCreateEmployer = false;
    $('#employersTable tbody').on('click', '#deleteEmployer', function () {
        tempEmployer.username = table.row($(this).parents('tr')).data()[0];
    });
};

/**
 * Delete department
 */
var deleteEmployer = function () {
    $.getJSON('/rest/deleteEmployer?username=' + tempEmployer.username, function (response) {
        if (response.hasOwnProperty("error")) {
            alert("error");
        } else {
            //$('#' + tempEmployer.username).html(tempDepartment.name);
        }
    });
    location.reload();
};

/**
 * Show update employer modal
 */
var updateEmployerModal = function () {
    isCreateEmployer = false;
    $('#employersTable tbody').on('click', '#updateEmployer', function () {
        tempEmployer.username = table.row($(this).parents('tr')).data()[0];
        tempEmployer.email = table.row($(this).parents('tr')).data()[1];
        tempEmployer.address = table.row($(this).parents('tr')).data()[2];
        tempEmployer.department = table.row($(this).parents('tr')).data()[3];
        tempEmployer.password = table.row($(this).parents('tr')).data()[4];
        tempEmployer.role = table.row($(this).parents('tr')).data()[5];
        $("#employerUsername").val(tempEmployer.username);
        $("#employerEmail").val(tempEmployer.email);
        $("#employerAddress").val(tempEmployer.address);
        $("#employerDepartment option[value="+tempEmployer.department+"]").attr('selected', 'selected');
        $("#employerRole option[value="+tempEmployer.role+"]").attr('selected', 'selected');
        $("#employerPassword").val(tempEmployer.password);
    });
};

/**
 * On modal open create
 */
var createEmployerModal = function () {
    isCreateEmployer = true;
    $("#employerUsername").val("");
    $("#employerEmail").val("");
    $("#employerAddress").val("");
    $("#employerDepartment option[value="+tempEmployer.department+"]").attr('selected', 'selected');
    $("#employerRole option[value="+tempEmployer.role+"]").attr('selected', 'selected');
    $("#employerPassword").val("");
};

/**
 * Show/hide password
 */
var showPassword =  function () {
    if ($('#employerPassword').attr('type') == "password") {
        $('#employerPassword').attr('type', 'text');
        $('#eye').addClass('fa-eye-slash').removeClass('fa-eye');
    } else {
        $('#employerPassword').attr('type', 'password');
        $('#eye').addClass('fa-eye').removeClass('fa-eye-slash');
    }
};

/**
 * Create or update an employer
 */
var createUpdateEmployer = function () {
    if (isCreateEmployer) {
        createEmployer();
    } else {
        updateEmplpyer();
    }
};

/**
 * Create new employer
 */
var createEmployer = function () {
    var newEmployerUsername = $("#employerUsername").val();
    var newEmployerEmail = $("#employerEmail").val();
    var newEmployerAddress = $("#employerAddress").val();
    var newEmployerDepartment = $("#employerDepartment").val();
    var newEmployerRole = $("#employerRole").val();
    var newEmployerPassword = $("#employerPassword").val();
    $.getJSON('/rest/addUser?username=' + newEmployerUsername + "&email=" + newEmployerEmail
        + "&address=" + newEmployerAddress + "&department=" + newEmployerDepartment
        + "&role=" + newEmployerRole + "&password=" + newEmployerPassword, function (response) {
        /*if (response.hasOwnProperty("error")) {
            alert("error");
        } else {
            $('#' + tempDepartment.id).html(newDepartmentName);
        }*/
    });
    location.reload();
};

/**
 * Update existing employer
 */
var updateEmplpyer = function () {
    var newEmployerUsername = $("#employerUsername").val();
    var newEmployerEmail = $("#employerEmail").val();
    var newEmployerAddress = $("#employerAddress").val();
    var newEmployerDepartment = $("#employerDepartment").val();
    var newEmployerRole = $("#employerRole").val();
    var newEmployerPassword = $("#employerPassword").val();
    $.getJSON('/rest/updateEmployer?username=' + newEmployerUsername + "&email=" + newEmployerEmail
        + "&address=" + newEmployerAddress + "&department=" + newEmployerDepartment
        + "&role=" + newEmployerRole + "&password=" + newEmployerPassword+ "&oldUsername="+tempEmployer.username, function (response) {
        /*if (response.hasOwnProperty("error")) {
         alert("error");
         } else {
         $('#' + tempDepartment.id).html(newDepartmentName);
         }*/
    });
    location.reload();
};

var getEmployersOnTreeClick = function (subDepartmentName) {
    $.getJSON("/rest/employers?subDepartmentName=" + subDepartmentName, function (response) {
        $('#empTableBody').empty();
        var employers = response;
        employers.forEach(function (empployer) {
            $('#empTableBody').append('<tr>'
                + '<td> ' + empployer.username + '</td>'
                + '<td> ' + empployer.email + '</td>'
                + '<td> ' + empployer.address + '</td>'
                + '<td> ' + empployer.department + '</td>'
                + '<td> ' + empployer.password + '</td>'
                + '<td> ' + empployer.role + '</td>'
                + '<td><button class="btn btn-primary" id="updateEmployer" onclick="updateEmployerModal()" aria-hidden="true" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-pencil-square-o"></i></button></td>'
                + '<td><button class="btn btn-danger " id="deleteEmployer" data-toggle="modal" data-target=".bs-example-modal-sm" onclick="deleteEmployerModal()"><i class="fa fa-trash" aria-hidden="true"></i></button></td></tr>'
            );
        });
        table.dataTable().fnDestroy();
        table = $('#employersTable').DataTable({
            "columnDefs": [
                {"visible": false, "targets": 4}
            ]
        });
    });
};