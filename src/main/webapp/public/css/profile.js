/**
 * Created by bruno on 24.4.2016.
 */
/**
 * Global user
 */
var user;

/**
 * Initial request to get user data
 */
$(document).ready(function () {
    $.getJSON('/rest/getEmployer', function (response) {
        var employer = response;
        user = response;
        $('#profileContainer').append('<div class="title">'
            + '<a href="">' + employer.username + '</a>'
            + '</div>'
            + '<div class="desc"><h5>Email: ' + employer.email + '</h5></div>'
            + '<div class="desc"><h5>Address: ' + employer.address + '</h5></div>'
            + '<div class="desc"><h5>Department: ' + employer.department + '</h5></div>');
    });
    var depName;
    $('#departmentTable tbody').on('click', 'tr', function () {
        depName = table.row(this).data()[0];
        window.location = "/employers?departmentName=" + depName;
    });
});

/**
 * Show update profile modal
 */
var updateProfileModal = function () {
    $("#profileUsername").val(user.username);
    $("#profileEmail").val(user.email);
    $("#profileAddress").val(user.address);
    $("#profileDepartment").val(user.department);
    $("#profilePassword").val(user.password);
};

/**
 * Update profile
 */
var updateProfile = function () {
    var newUser = user;
    newUser.username = $("#profileUsername").val();
    newUser.email = $("#profileEmail").val();
    newUser.address = $("#profileAddress").val();
    newUser.department = $("#profileDepartment").val();
    newUser.password = $("#profilePassword").val();
    $.getJSON('/rest/updateProfile?username=' + newUser.username + "&email=" + newUser.email
        + "&address=" + newUser.address + "&department=" + newUser.department + "&password=" + newUser.password, function (response) {
        if (response.hasOwnProperty("error")) {
            console.log(response)
        } else {
            alert(success);
        }
    });
    location.reload();
};

/**
 * Show/Hide password
 */
var showPassword =  function () {
    if ($('#profilePassword').attr('type') == "password") {
        $('#profilePassword').attr('type', 'text');
        $('#eye').addClass('fa-eye-slash').removeClass('fa-eye');
    } else {
        $('#profilePassword').attr('type', 'password');
        $('#eye').addClass('fa-eye').removeClass('fa-eye-slash');
    }
};




