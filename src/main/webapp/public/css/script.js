/**
 * Created by bruno on 25.4.2016.
 */

/*
 GLOBAL VARIABLE DECLARATION
 */
var table;
var isCreateDepartament = true;
var tempDepartment = {name: "", id:-1};

/**
 * Open modal for department create
 */
var createDepartmentModal = function () {
    isCreateDepartament = true;
};

/**
 * Open modal for department update
 */
var updateDepartamentModal = function () {
    isCreateDepartament = false;
    $('#departmentTable tbody').on('click', '#updateDepartment', function () {
        tempDepartment.Id = table.row($(this).parents('tr')).data()[0];
        tempDepartment.name = table.row($(this).parents('tr')).data()[1];
        $('#departmentNameField').val(tempDepartment.name);
    });
};

/**
 * Chose if is create or update on
 */
var createUpdateDepartment = function () {
    if (isCreateDepartament) {
        createDepartment(name);
    } else {
        updateDepartment();
    }
};

/**
 * Create new department
 */
var createDepartment = function () {
    var newDepartmentName = $('#departmentNameField').val();
    $.getJSON('/rest/createDepartment?name=' + newDepartmentName, function (response) {
        if (response.hasOwnProperty("error")) {
            alert("error");
        } else {
            $('#' + tempDepartment.id).html(newDepartmentName);
        }
    });
    location.reload();
};

/**
 * Update department
 */
var updateDepartment = function () {
    var newDepartmentName = $('#departmentNameField').val();
    $.getJSON('/rest/updateDepartment?id=' + tempDepartment.Id + "&name=" + newDepartmentName, function (response) {
        if (response.hasOwnProperty("error")) {
            alert("error");
            $('#myModal').modal('hide');
        } else {
            $('#' + tempDepartment.id).html(tempDepartment.name);
        }
    });
    location.reload();
};


/**
 * Open delete modal
 */
var deleteDepartmentModal = function () {
    isCreateDepartament = false;
    $('#departmentTable tbody').on('click', '#deleteDepartment', function () {
        tempDepartment.Id = table.row($(this).parents('tr')).data()[0];
        tempDepartment.name = table.row($(this).parents('tr')).data()[1];
        $('#departmentNameField').val(tempDepartment.name);
    });
};

/**
 * Delete department
 */
var deleteDepartment = function() {
    $.getJSON('/rest/deleteDepartment?id=' + tempDepartment.Id, function (response) {
        if (response.hasOwnProperty("error")) {
            alert("error");
        } else {
            $('#' + tempDepartment.id).html(tempDepartment.name);
        }
    });
    location.reload();
};

/**
 * Get departments on page load
 */
$(document).ready(function () {
    $.getJSON('/rest/department', function (response) {
        var departaments = response;
        departaments.forEach(function (department) {
            $('#depTableBody').append('<tr><td>' + department.id + '</td><td id="' + department.id + '">' + department.name + '</td>'
                + '<td><button class="btn btn-primary" id="updateDepartment" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="updateDepartamentModal()">' +
                '<i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>'
                + '<td><button id="deleteDepartment" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm" onclick="deleteDepartmentModal()"><i class="fa fa-trash" aria-hidden="true"></i></button></td></tr>');
        });
        table = $('#departmentTable').DataTable({
            "columnDefs": [
                {"visible": false, "targets": 0}
            ]
        });
    });
    var depName;
    $('#departmentTable tbody').on('click', 'tr', function () {
        if (isCreateDepartament) {
            depName = table.row(this).data()[1];
            window.location = "/employers?departmentName=" + depName;
        }
    });
});

/**
 * Link me to employers page
 */
var getAllEmployers = function () {
    window.location = "/employers";
};

/**
 * Link me to profile page
 */
var profile = function () {
    window.location = "/profile";
};

/**
 * Link me to departments page
 */
var getAllDepartment = function () {
    window.location = "/department";
};

/**
 * On modal close
 */
$(document).on('hidden.bs.modal', function () {
    isCreateDepartament = true;
    $('#departmentNameField').val('');
});


JSONTest = function () {

    var resultDiv = $("#resultDivContainer");

    $.ajax({
        url: "https://example.com/api/",
        type: "POST",
        data: {apiKey: "23462", method: "example", ip: "208.74.35.5"},
        dataType: "json",
        success: function (result) {
            switch (result) {
                case true:
                    processResponse(result);
                    break;
                default:
                    resultDiv.html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
};
