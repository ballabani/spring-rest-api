package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 *
 */

@SpringBootApplication
@ComponentScan
@Configuration
@EnableAutoConfiguration
@EnableAsync
public class Application {
    /**
     * Metoda qe exekutohet dhe ben deploy app ne server
     * Serveri esht tomcat dhe eshte i inkorporuar brenda frameworkut kshu qe nuk ke nevoj (localhost:8080)
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
