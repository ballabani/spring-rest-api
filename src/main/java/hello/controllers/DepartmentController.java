package hello.controllers;

import hello.entities.Department;
import hello.entities.Employer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bruno on 25.4.2016.
 */

@RestController
public class DepartmentController {

    /**
     * Get all departments
     *
     * @return
     */
    @RequestMapping(value = "/rest/department", method = RequestMethod.GET)
    public List<Department> getDepartmnets() {

        String query = "SELECT * FROM `deparment`";
        List<Department> departments = new ArrayList<Department>();
        try {
            Db connect = new Db();
            Connection connection;
            Statement stmt;
            connection = connect.getConnection();
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            while (resultSet.next()) {
                // Retrieve by column name
                Department department = new Department();
                department.setName(resultSet.getString("name"));
                department.setId(resultSet.getInt("ID"));
                departments.add(department);

            }
            resultSet.close();
            connection.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return departments;
    }


    @RequestMapping(value = "/rest/updateDepartment", method = RequestMethod.GET)
    public String updateDepartment(
            @RequestParam(value="name", defaultValue="") String name,
            @RequestParam(value="id", defaultValue="") String id
    ) {
        String query = "UPDATE `deparment` SET name=? WHERE id = ?";
        Db connect = new Db();
        Connection conn = null;
        PreparedStatement stmt = null;
        conn = connect.getConnection();
        try{
            stmt = conn.prepareStatement(query);

            stmt.setString(1, name);
            stmt.setString(2, id);


            stmt.execute();


        }catch (Exception e){
            return "{error: 'update error exception'}";
        }
        return "{ id:" + id + ", name: " + name + "}";
    }

    @RequestMapping(value = "/rest/createDepartment", method = RequestMethod.GET)
    public String createDepartment(
            @RequestParam(value="name", defaultValue="") String name
    ) {
        String query = "INSERT INTO `deparment`(name) VALUES (?)";
        Db connect = new Db();
        Connection conn = null;
        PreparedStatement stmt = null;
        conn = connect.getConnection();
        try{
            stmt = conn.prepareStatement(query);

            stmt.setString(1, name);

            stmt.execute();


        }catch (Exception e){
            return "{error: 'insert error exception'}";
        }
        return "{name: " + name + "}";
    }

    @RequestMapping(value = "/rest/deleteDepartment", method = RequestMethod.GET)
    public String deleteDepartment(
            @RequestParam(value="id", defaultValue="") String id
    ) {
        String query = "DELETE FROM `deparment` WHERE id=?";
        Db connect = new Db();
        Connection conn = null;
        PreparedStatement stmt = null;
        conn = connect.getConnection();
        try{
            stmt = conn.prepareStatement(query);
            stmt.setString(1, id);
            stmt.execute();

        }catch (Exception e){
            return "{error: 'insert error exception'}";
        }
        return "{id: " + id + "}";
    }

    public static int getDepartmentID(String department) {
        int dep = 0;
        String query = "Select id FROM deparment where name=?";
        Db connect = new Db();
        Employer employer = new Employer();
        PreparedStatement stmt = null;
        Connection conn = null;
        conn = connect.getConnection();
        try {
            stmt = conn.prepareStatement(query);
            stmt.setString(1, department);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                dep = rs.getInt("id");
            }
        } catch (Exception e) {
        }
        return dep;
    }
    @RequestMapping(value = "/rest/getSubDepartment", method = RequestMethod.GET)
    public List<Department>getSubDepartment(@RequestParam(value="departmentName", defaultValue="")String departmentName) {
        int id = getDepartmentID(departmentName);
        String query = "SELECT * FROM `subdepartment` WHERE depid=?";

            List<Department> departments = new ArrayList<Department>();
            try {
                Db connect = new Db();
                Connection connection;
                PreparedStatement stmt = null;
                connection = connect.getConnection();

                stmt = connection.prepareStatement(query);
                stmt.setInt(1,id);
                ResultSet resultSet = stmt.executeQuery();
                while (resultSet.next()) {
                    // Retrieve by column name
                    Department department = new Department();
                    department.setName(resultSet.getString("name"));
                    department.setId(resultSet.getInt("id"));
                    departments.add(department);
                }
                resultSet.close();
                connection.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
return departments;


    }
}

