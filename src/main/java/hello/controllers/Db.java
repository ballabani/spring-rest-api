/*
 * Classname
 * 
 * Version information
 *
 * Date
 * 
 * Copyright � Bruno Salla
 */
package hello.controllers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Db {
	private Connection connection;

	public Db() {
		openConnection();
	}

	/**
	 * Open the connection with the database
	 */
	private void openConnection() {
		try {
			connection = DriverManager.getConnection(
					Constancts.Db.PATH + Constancts.Db.DB, Constancts.Db.USER,
					Constancts.Db.PASSWORD);

			System.out.println(Constancts.Messages.SUCCESS);

		} catch (SQLException e) {
			System.out.println("fail" + e.getLocalizedMessage());
			e.getErrorCode();
		}

	}

	/**
	 * Close the connection with database.
	 */
	public void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Get the connection with the database
	 * 
	 * @return
	 */
	public Connection getConnection() {
		if (connection == null) {
			openConnection();
		}
		return connection;

	}
}
