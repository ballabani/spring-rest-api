package hello.controllers;

import hello.entities.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Project: real
 * Package: hello.controllers
 * Author: bruno
 * Created Date: 4/22/2016 2:54 PM
 *
 */

@RestController
public class UserController {

    @RequestMapping("/user")
    public User greeting(
            @RequestParam(value="username", defaultValue="username") String username,
            @RequestParam(value="password", defaultValue = "no password") String password
    ) {
        return new User(username, password);
    }
}
