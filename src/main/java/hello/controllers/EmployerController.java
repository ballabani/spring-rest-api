package hello.controllers;

import hello.entities.Employer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bruno on 25.4.2016.
 */
@org.springframework.web.bind.annotation.RestController
public class EmployerController {
    //private Employer employer;
//private Employer

    @RequestMapping("/rest/addUser")
    public void addEmployer(@RequestParam(value = "username", defaultValue = "username") String username,
                            @RequestParam(value = "password", defaultValue = "password") String password,
                            @RequestParam(value = "address", defaultValue = "address") String address,
                            @RequestParam(value = "department", defaultValue = "departmentId") String departmentId,
                            @RequestParam(value = "email", defaultValue = "user@gmai.com") String email,
                            @RequestParam(value = "role", defaultValue = "role") String role

    ) {
        DepartmentController department = new DepartmentController();
        String sql = "INSERT into`users`(username,password,email,address,department_id)values(?, ?, ? , ?, ?) ";
        String sql1 = "INSERT into`user_roles`(username, role)values(?, ?) ";
        Db connect = new Db();
        Connection conn = null;
        PreparedStatement stmt = null;
        conn = connect.getConnection();
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            stmt.setString(2, password);
            stmt.setString(3, email);
            stmt.setString(4, address);
            stmt.setInt(5, department.getDepartmentID(departmentId));

            stmt.execute();
            stmt = conn.prepareStatement(sql1);
            stmt.setString(1, username);
            stmt.setString(2, role);
            stmt.execute();
            conn.close();
            stmt.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    /**
     * Update employer from admin !
     */

    @RequestMapping("/rest/updateEmployer")
    public void updateEmployer(@RequestParam(value = "username", defaultValue = "username") String username,
                               @RequestParam(value = "password", defaultValue = "password") String password,
                               @RequestParam(value = "address", defaultValue = "address") String address,
                               @RequestParam(value = "department", defaultValue = "departmentId") String departmentName,
                               @RequestParam(value = "email", defaultValue = "user@gmai.com") String email,
                               @RequestParam(value = "role", defaultValue = "ROLE_USER") String role,
                               @RequestParam(value = "oldUsername", defaultValue = "ROLE_USER") String oldUsername

    ) {
        DepartmentController department = new DepartmentController();
        int depId = DepartmentController.getDepartmentID(departmentName);
        String sql = "Update `users` SET username = ?, password = ?, address = ?, email = ? , department_id = ? WHERE username = ?";
        String sql1 = "UPDATE `user_roles` SET `role`=? WHERE username=? ";
        Db connect = new Db();
        Connection conn = null;
        PreparedStatement stmt = null;
        conn = connect.getConnection();
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            stmt.setString(2, password);
            stmt.setString(3, address);
            stmt.setString(4, email);
            stmt.setInt(5, depId);
            stmt.setString(6, oldUsername);

            stmt.execute();
            stmt = conn.prepareStatement(sql1);
            stmt.setString(2, oldUsername);
            stmt.setString(1, role);
            stmt.execute();
            conn.close();
            stmt.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    /**
     * Update profile from simple user
     */

    @RequestMapping("/rest/updateProfile")
    public void updateProfile(@RequestParam(value = "username", defaultValue = "username") String username,
                              @RequestParam(value = "password", defaultValue = "password") String password,
                              @RequestParam(value = "address", defaultValue = "address") String address,
                              @RequestParam(value = "department", defaultValue = "departmentId") String departmentName,
                              @RequestParam(value = "email", defaultValue = "user@gmai.com") String email

    ) {
        DepartmentController department = new DepartmentController();
        int depId = DepartmentController.getDepartmentID(departmentName);
        String sql = "Update `users` SET username = ?, password = ?, address = ?, email = ? , department_id = ? WHERE username = ?";
        Db connect = new Db();
        Connection conn = null;
        PreparedStatement stmt = null;
        conn = connect.getConnection();
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            stmt.setString(2, password);
            stmt.setString(3, address);
            stmt.setString(4, email);
            stmt.setInt(5, depId);
            stmt.setString(6, username);

            stmt.execute();
            conn.close();
            stmt.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    /**
     * Delete employer
     */

    @RequestMapping(value = "/rest/deleteEmployer", method = RequestMethod.GET)
    public String removEmployer(
            @RequestParam(value = "username", defaultValue = "") String username
    ) {
        String sql = " DELETE FROM `users` WHERE  username=?";
        String roleSql = " DELETE FROM `user_roles` WHERE  username=?";
        Db connect = new Db();
        Connection conn = null;
        PreparedStatement stmt = null;
        conn = connect.getConnection();
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            stmt.execute();
            conn.close();
            stmt.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "{username: " + username + "}";
    }

    private String getUserDepartment(String user) {

        String deptName1 = null;

        String query = "SELECT name FROM deparment INNER JOIN users ON deparment.ID=users.department_id WHERE username=? ";
        Db connect = new Db();
        Employer employer = new Employer();
        PreparedStatement stmt = null;
        Connection conn = null;
        conn = connect.getConnection();
        try {
            stmt = conn.prepareStatement(query);
            stmt.setString(1, user);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {

                deptName1 = rs.getString("name");
            }
        } catch (Exception e) {
        }

        return deptName1;
    }

    @RequestMapping(value = "/rest/employers", method = RequestMethod.GET)
    public List<Employer> getEmployerList(
            @RequestParam(value = "departmentName", defaultValue = "departmentName") String departmentName,
            @RequestParam(value = "subDepartmentName", defaultValue = "subDepartmentName") String subDepartmentName
    ) {
        Db connect = new Db();
        Connection conn = null;

        PreparedStatement stmt = null;

        conn = connect.getConnection();
        List<Employer> employers = new ArrayList<Employer>();


        try {
            String query = "";
            boolean hasParam = false;
            boolean hasSub = false;
            if (departmentName != null && !departmentName.equals("departmentName")) {
                query = "SELECT * FROM `users` " +
                        "INNER JOIN deparment ON deparment.id=users.department_id" +
                        " INNER JOIN `user_roles` ON user_roles.username=users.username WHERE deparment.name=?";
                hasParam = true;
            } else if (subDepartmentName != null && !subDepartmentName.equals("subDepartmentName")) {
                query = "SELECT * FROM `users` "
                        + "INNER JOIN subdepartment ON subdepartment.id = users.subDepartmentId "
                        + "INNER JOIN deparment ON deparment.id=users.department_id "
                        + " INNER JOIN `user_roles` ON user_roles.username=users.username WHERE subdepartment.name=?";
                hasSub = true;
            } else {
                query = "SELECT * FROM `users` INNER JOIN deparment ON deparment.id=users.department_id INNER JOIN `user_roles` ON user_roles.username=users.username";
            }
            stmt = conn.prepareStatement(query);
            if (hasParam) {
                stmt.setString(1, departmentName);
            } else if (hasSub) {
                stmt.setString(1, subDepartmentName);
            }

            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                // Retrieve by column name
                Employer employer = new Employer();
                employer.setUsername(resultSet.getString("username"));
                employer.setPassword(resultSet.getString("password"));
                employer.setAddress(resultSet.getString("address"));
                employer.setEmail(resultSet.getString("email"));
                employer.setDepartment(resultSet.getString("name"));
                employer.setRole(resultSet.getString("role"));
                employers.add(employer);

            }
            resultSet.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return employers;
    }

    @RequestMapping(value = "/rest/getEmployer", method = RequestMethod.GET)
    public Employer getEmployer(HttpServletRequest h) {


        String query = "SELECT * FROM users WHERE username= ? ";
        Db connect = new Db();
        Connection conn = null;
        Employer employer = new Employer();
        PreparedStatement stmt = null;
        conn = connect.getConnection();
        try {
            stmt = conn.prepareStatement(query);
            stmt.setString(1, h.getRemoteUser());

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                //  HttpServletRequest.getUserPrincipal();
                employer.setUsername(rs.getString("username"));
                employer.setPassword(rs.getString("password"));
                employer.setAddress(rs.getString("address"));
                employer.setEmail(rs.getString("email"));
                employer.setDepartment(getUserDepartment(h.getRemoteUser()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employer;

    }

    @RequestMapping("rest/updateUser")
    public void updateUser(
            @RequestParam(value = "password", defaultValue = "password") String password,
            @RequestParam(value = "address", defaultValue = "address") String address,
            @RequestParam(value = "email", defaultValue = "user@gmai.com") String email


    ) {


        String query = "UPDATE `users` SET password=?,`email`=?,`address`= ? WHERE 1 ";
        Db connect = new Db();
        Connection conn = null;
        Employer employer = new Employer();
        PreparedStatement stmt = null;
        conn = connect.getConnection();
        try {
            stmt = conn.prepareStatement(query);

            stmt.setString(1, password);
            stmt.setString(2, email);
            stmt.setString(3, address);


            stmt.execute();


        } catch (Exception e) {

        }

    }

}


