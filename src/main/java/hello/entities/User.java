package hello.entities;

/**
 * Project: real
 * Package: hello.entities
 * Author: Marjo
 * Created Date: 4/22/2016 2:52 PM
 * This code is copyright (c) 2016 Prius Solution
 */
public class User {
    private final String username;
    private final String password;

public User(){
    password=null;
    username=null;
}
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
