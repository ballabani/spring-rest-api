package hello.entities;

/**
 * Created by bruno on 25.4.2016.
 */
public class Department {

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
