package hello.entities;

/**
 * Created by bruno on 25.4.2016.
 */
public class Employer {
public  Employer(){

}

    public Employer(String address, String username, String department, String email, String name, String password, String role) {
        this.address = address;
        this.username = username;
        this.department = department;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    private String address;
    private String email;
    private String username;
    private String password;
    private String department;
    private String role;

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }



}
